#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"
#include "time.h"

struct datetime get_datetime_from_timestamp(uint timestamp)
{
  struct datetime _time;
  _time.ticks = timestamp & 0x7F;
  _time.second = (timestamp >> 7) & 0x3F;
  _time.minute = (timestamp >> 13) & 0x3F;
  _time.hour = (timestamp >> 19) & 0x1F;
  return _time;
}

int
main(int argc, char *argv[])
{
  int i = 0, count = 0;
  struct datetime _date;
  uint ocp, total;

  if(argc > 1)
  {
    if(strcmp(argv[1], "-empty") == 0 || strcmp(argv[1], "-e") == 0)
    {
      recycle_empty();
    }
    else if(strcmp(argv[1], "-list") == 0 || strcmp(argv[1], "-l") == 0)
    {
      struct recyclebin_file rf[10];
      recycle_list(rf);
      for(i=0; i<10; i++) if(rf[i].id > 0) count++;
      printf(1," ====== Recycle Bin ====== \n");
      printf(1,"Total files in recycle bin : %d files\n", count);
      for(i=0; i<10; i++)
      {
        if(rf[i].id > 0)
        {
          _date = get_datetime_from_timestamp(rf[i].timestamp);
          printf(1, "[%d] Filename : %s\tSize = %d bytes\tDeleted time : %d:%d:%d\n",rf[i].id ,rf[i].name, rf[i].size, _date.hour, _date.minute, _date.second);
        }
      }
      recycle_getstatus(&total, &ocp);
      printf(1,"Total size %d bytes.\tUsed size %d bytes [%d %]\n", total, ocp, ocp * 100 / total);
      printf(1," ====== Recycle Bin ====== \n");
    }
    else if((strcmp(argv[1], "-restore") == 0 || strcmp(argv[1], "-r") == 0) && argc > 2)
    {
      int id = atoi(argv[2]);
      recycle_restore(id);
    }
  }
  // else
    // printf(1,"recycle files = %d\n", recycle_list());
  exit();
}