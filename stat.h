#include "time.h"

#define T_DIR  1   // Directory
#define T_FILE 2   // File
#define T_DEV  3   // Device
#define T_SYMLINK   4 // Symlink
#define T_RECYCLED	5	// Recycle Bin

struct stat {
  short type;  // Type of file
  int dev;     // File system's disk device
  uint ino;    // Inode number
  short nlink; // Number of links to file
  uint size;   // Size of file in bytes
  short attr;
};

struct recyclebin_file
{
  uint id;
  char name[14];
  uint size;
  uint timestamp;
  struct datetime deleted_time;
};
