#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"
#include "time.h"

int
main(int argc, char *argv[])
{
	if(argc < 3)
		printf(1,"chmod [mode] [file]\n");
	else
	{
		chmod(atoi(argv[1]), argv[2]);
	}
	exit();
}