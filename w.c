#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"

char buf[8192];
char name[3];
char *echoargv[] = { "echo", "ALL", "TESTS", "PASSED", 0 };
int stdout = 1;

void
bigfile(void)
{
  int fd, i, total, cc;
  
  // uint target_size = 102400;

  printf(1, "bigfile test\n");

  unlink("bigfile");
  fd = open("bigfile", O_CREATE | O_RDWR);
  if(fd < 0){
    printf(1, "cannot create bigfile");
    exit();
  }
  for(i = 0; i < 100; i++){
    memset(buf, i, 600);
    if(write(fd, buf, 600) != 600){
      printf(1, "write bigfile failed\n");
      exit();
    }
  }
  close(fd);

  fd = open("bigfile", 0);
  if(fd < 0){
    printf(1, "cannot open bigfile\n");
    exit();
  }
  
  total = 0;
  
  for(i = 0; ; i++){
    cc = read(fd, buf, 300);
    if(cc < 0){
      printf(1, "read bigfile failed\n");
      exit();
    }
    if(cc == 0)
      break;
    if(cc != 300){
      printf(1, "short read bigfile\n");
      exit();
    }
    if(buf[0] != i/2 || buf[299] != i/2){
      printf(1, "read bigfile wrong data\n");
      exit();
    }
    total += cc;
  }
  close(fd);
  if(total != 60000){
    printf(1, "read bigfile wrong total\n");
    exit();
  }

  printf(1, "bigfile test ok\n");
}

void 
find_big_file_size()
{
  int i, fd;
  printf(1, "find max file target_size test\n");

  
  strcpy(buf,"hello");
  unlink("bigfile");
  fd = open("bigfile", O_CREATE | O_RDWR);
  if(fd < 0){
    printf(1, "cannot create bigfile");
    exit();
  }
  for(i=0;;i++)
  {
    // memset(buf, 1, 512);
    if(write(fd, buf, 512) < 0){
      break;      
    }
  }
  close(fd);
  printf(1, "max file %d bytes!\n", i*512);
  printf(1, "find max file target_size test completed!\n");
}


void
write_file(int n)
{
  int i, fd, j, l;
  char name[10];
  printf(1, "write %d files.\n",n);
  strcpy(name, "tmp");

  for(j=0; j<n; j++)
  {
    l = j;
    name[6] = 0;
    name[5] = (l%10)+'0'; l = l / 10; 
    name[4] = (l%10)+'0'; l = l / 10; 
    name[3] = (l%10)+'0'; l = l / 10; 

    unlink(name);
    fd = open(name, O_CREATE | O_RDWR);
    if(fd < 0){
      printf(1, "cannot create %s", name);
      exit();
    }
    for(i=0; i<5; i++)
    {
      memset(buf, 1, 512);
      if(write(fd, buf, 512) < 0){
        break;      
      }
    }
    close(fd);
  }
  
  printf(1, "write %d files completed!\n", j);
}

void
write_big_file(int n)
{
  int i, fd, j;
  char name[10];
  printf(1, "write %d files. [max 10 files]\n",n);
  strcpy(name, "bigtmp");

  for(j=0; j<n && j<10; j++)
  {
    printf(1,".");
    name[6] = j+'0';
    name[7] = 0;

    unlink(name);
    fd = open(name, O_CREATE | O_RDWR);
    if(fd < 0){
      printf(1, "cannot create %s", name);
      exit();
    }
    for(i=0; i<140; i++)
    {
      memset(buf, 1, 512);
      if(write(fd, buf, 512) < 0){
        break;      
      }
    }
    close(fd);
  }
  
  printf(1, "\nwrite %d files completed!\n", j);
}

int
main(int argc, char *argv[])
{
  // bigfile();
  if(argc > 2)
  {
    if(strcmp(argv[1], "-m") == 0 && atoi(argv[2]) > 0)
    {
      printf(1,"Write %d big files\n", atoi(argv[2]));
      write_big_file(atoi(argv[2]));
    }
  }
  else if(argc > 1)
  {
    write_file(atoi(argv[1]));
  }
  else 
  {
    find_big_file_size();
  }
  exit();
}