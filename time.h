#ifndef TIME_H
#define TIME_H

struct datetime
{
  uint ticks;
  uint second;
  uint minute;
  uint hour;
};

// time
struct datetime get_datetime_from_timestamp(uint timestamp);
uint 			get_timestamp_from_datetime(struct datetime _time);
uint 			get_current_timestamp();

#endif